//
//  AppDelegate.h
//  tapnshare
//
//  Created by Aleks Mutlu on 21/10/14.
//  Copyright (c) 2014 Aleks Mutlu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

